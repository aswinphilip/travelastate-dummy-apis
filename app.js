const server = require('express');

const app = server();

app.use('/', require('./routes'));

app.listen(8082, () => {
    console.log('App listening on http://localhost:8082');
});